import scrapy


class HvnSpider(scrapy.Spider):
    name = 'hvn'
    # allowed_domains = ['havan.com']
    # start_urls = [f'https://www.havan.com.br/ofertas?p={i}' for i in range(1,6)]
    start_urls = ['https://www.havan.com.br/ofertas?p=1']

    def parse(self, response, **kwargs):

        for i in response.xpath('.//li[@class="item product product-item "]'): 
            nome = i.xpath('.//div[@class="product details product-item-details"]//strong[@class="product name product-item-name "]/h2/a/text()').get()
            preco = i.xpath('.//div[@class="price-box price-final_price"]//span[@class="special-price"]//span[@class="price"]/text()').get()

            yield{
                'nome':nome.encode().decode('UTF-8').replace('\n','')
                .replace('    ','')
                .replace('                            ',''),
                'preço':preco, 
            }

        next_page = response.xpath('//a[contains(@title, "Próximo")]/@href').get()
        if next_page:
            yield scrapy.Request(url = next_page, callback = self.parse)
